DECLARE

	CURSOR c IS SELECT table_name FROM user_tables;
	v_nom_table user_tables.table_name%type;
	req VARCHAR2(200);

BEGIN

	OPEN c;
	LOOP

		FETCH c INTO v_nom_table;

		IF v_nom_table NOT LIKE '%!_old' ESCAPE '!' THEN

			req := 'CREATE TABLE ' || v_nom_table || '_old AS SELECT * FROM ' || v_nom_table || ';';
			dbms_output.put_line(req);
			execute immediate req;

		END IF;

	END LOOP;
	CLOSE c;

END;
/