CREATE OR REPLACE PROCEDURE createdept_julliouf(numero_dept IN dept.deptno%type, dept_name IN dept.dname%type, localisation IN dept.loc%type)
IS

	v_existe number;

BEGIN

	SELECT COUNT(*) INTO v_existe FROM dept WHERE deptno = numero_dept;

	IF v_existe = 0 THEN

		INSERT INTO dept(deptno, dname, loc) VALUES (numero_dept, dept_name, localisation);
		COMMIT;

	ELSE

		RAISE_APPLICATION_ERROR(-20404, 'Ce numéro de dept existe déjà !');

	END IF;

END createdept_julliouf;
/