CREATE OR REPLACE PROCEDURE raisesalary_julliouf(p_emp_id IN emp.empno%type, p_amount IN emp.sal%type)
IS

	v_salaire_augmente	emp.sal%type;
	v_job				emp.job%type;
	v_salaire_min		emp.sal%type;
	v_salaire_max		emp.sal%type;

BEGIN

	SELECT sal INTO v_salaire_augmente FROM emp WHERE empno = p_emp_id;

	v_salaire_augmente := v_salaire_augmente + p_amount;

	dbms_output.put_line(v_salaire_augmente);

	SELECT job	INTO v_job			FROM emp				WHERE empno = p_emp_id;
	SELECT lsal	INTO v_salaire_min	FROM SalIntervalle_F2	WHERE job = v_job;
	SELECT hsal	INTO v_salaire_max	FROM SalIntervalle_F2	WHERE job = v_job;

	IF v_salaire_augmente BETWEEN v_salaire_min AND v_salaire_max THEN

		UPDATE emp SET sal = v_salaire_augmente WHERE empno = p_emp_id;

	ELSE

		RAISE_APPLICATION_ERROR(-20404, 'Une erreur sest produite !');

	END IF;

END;
/