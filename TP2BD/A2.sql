CREATE OR REPLACE FUNCTION salok_maxiouf(p_job IN SalIntervalle_F2.job%type, p_salaire IN SalIntervalle_F2.lsal%type)
RETURN NUMBER
IS

	v_borne1 SalIntervalle_F2.lsal%type;
	v_borne2 SalIntervalle_F2.lsal%type;
	CURSOR c IS SELECT lsal, hsal FROM SalIntervalle_F2 s WHERE job = p_job;

BEGIN

	OPEN c;
	FETCH c INTO v_borne1, v_borne2;
	CLOSE c;

	IF p_salaire >= v_borne1 AND p_salaire <= v_borne2 THEN
		RETURN 1;
	END IF;

	RETURN 0;

END salok_maxiouf;
/