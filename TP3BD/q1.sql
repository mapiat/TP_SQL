CREATE TRIGGER sal_emp
BEFORE UPDATE OF sal ON emp
FOR EACH ROW
BEGIN

	IF (:new.sal < :old.sal) THEN

		RAISE_APPLICATION_ERROR(-20224, 'Le salaire ne peut diminuer.');

	END IF;

END;
/