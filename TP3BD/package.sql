CREATE OR REPLACE PACKAGE pkg AS
	TYPE num_nom_emp IS RECORD (c_empno emp.empno%type, c_ename emp.ename%type);
	CURSOR emp_par_dep_maxiouf(deptno dept.deptno%type) RETURN num_nom_emp;
	PROCEDURE raise_salary_maxiouf(p_emp_id IN emp.empno%type, p_amount IN emp.sal%type);
	PROCEDURE afficher_emp_maxiouf(deptno IN dept.deptno%type);
END pkg;
/