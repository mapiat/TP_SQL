

CREATE OR REPLACE PACKAGE BODY pkg AS

	CURSOR emp_par_dep_maxiouf(deptno dept.deptno%type) RETURN num_nom_emp IS
		SELECT empno, ename FROM emp e WHERE e.deptno = deptno;

	PROCEDURE raise_salary_maxiouf(p_emp_id IN emp.empno%type, p_amount IN emp.sal%type)
	IS

		v_salaire_augmente	emp.sal%type;
		v_job				emp.job%type;
		v_salaire_min		emp.sal%type;
		v_salaire_max		emp.sal%type;

	BEGIN

		SELECT sal INTO v_salaire_augmente FROM emp WHERE empno = p_emp_id;

		v_salaire_augmente := v_salaire_augmente + p_amount;

		dbms_output.put_line(v_salaire_augmente);

		SELECT job	INTO v_job			FROM emp				WHERE empno = p_emp_id;
		SELECT lsal	INTO v_salaire_min	FROM SalIntervalle_F2	WHERE job = v_job;
		SELECT hsal	INTO v_salaire_max	FROM SalIntervalle_F2	WHERE job = v_job;

		IF v_salaire_augmente BETWEEN v_salaire_min AND v_salaire_max THEN

			UPDATE emp SET sal = v_salaire_augmente WHERE empno = p_emp_id;

		ELSE

			RAISE_APPLICATION_ERROR(-20404, 'Une erreur sest produite !');

		END IF;

	END;

	PROCEDURE afficher_emp_maxiouf(deptno IN dept.deptno%type)
	IS
		v_empno	emp.empno%type;
		v_ename emp.ename%type;
	BEGIN
		OPEN emp_par_dep_maxiouf(deptno);
		LOOP
			FETCH emp_par_dep_maxiouf INTO v_empno, v_ename;
			dbms_output.put_line('Lemployé numéro : ' || v_empno || ' se nomme ' || v_ename);
		END LOOP;
		CLOSE emp_par_dep_maxiouf;
	END;

END pkg;
/