CREATE OR REPLACE TRIGGER nouv_job
BEFORE UPDATE ON emp
FOR EACH ROW
DECLARE

	v_hsal		SalIntervalle_F2.hsal%type;
	v_lsal		SalIntervalle_F2.lsal%type;

BEGIN

	IF :new.job != 'PRESIDENT' THEN

		:new.sal := :old.sal + 100;

	END IF;

	SELECT hsal, lsal INTO v_hsal, v_lsal FROM SalIntervalle_F2 WHERE job = :new.job;

	IF v_lsal > :new.sal THEN
		:new.sal := v_lsal;
	ELSIF :new.sal > v_hsal THEN
		:new.sal := v_hsal;
	END IF;

END;
/