CREATE OR REPLACE TRIGGER dep_61_69
BEFORE INSERT OR UPDATE OF deptno ON dept
FOR EACH ROW
BEGIN

	IF NOT (61 <= :new.deptno AND :new.deptno <= 69) THEN

		RAISE_APPLICATION_ERROR(-20001, 'Département incorrect.');

	END IF;

END;
/