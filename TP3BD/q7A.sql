CREATE OR REPLACE TRIGGER emp_stats
AFTER INSERT OR UPDATE OR DELETE ON emp
FOR EACH ROW
BEGIN

	IF deleting THEN

		 UPDATE STATS_maxiouf SET NbMaj = NbMaj + 1, Date_derniere_Maj = SYSDATE WHERE TypeMaj = 'DELETE';

	ELSIF inserting THEN

		UPDATE STATS_maxiouf SET NbMaj = NbMaj + 1, Date_derniere_Maj = SYSDATE WHERE TypeMaj = 'INSERT';

	ELSIF updating THEN

		UPDATE STATS_maxiouf SET NbMaj = NbMaj + 1, Date_derniere_Maj = SYSDATE WHERE TypeMaj = 'UPDATE'; 

	END IF;

END;
/