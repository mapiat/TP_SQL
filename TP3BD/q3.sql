CREATE OR REPLACE TRIGGER dep_emp
BEFORE INSERT OR UPDATE OF deptno ON emp
FOR EACH ROW
DECLARE
	v_exist NUMBER;
BEGIN

	SELECT COUNT(deptno) INTO v_exist FROM dept WHERE deptno = :new.deptno;

	IF (v_exist = 0) THEN

		INSERT INTO dept VALUES(:new.deptno, 'A SAISIR', 'A SAISIR');

	END IF;

END;
/