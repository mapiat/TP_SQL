DECLARE
	v_ename	VARCHAR(25);
	v_sal	NUMBER;
	v_comm	NUMBER;
	v_dname	VARCHAR(25);

BEGIN

	SELECT ename, sal, comm, dname INTO v_ename, v_sal, v_comm, v_dname FROM emp e
		JOIN dept d on e.deptno = d.deptno
		WHERE ename='MILLER';
	
	dbms_output.put_line(v_ename || ' ' || v_sal || ' ' || v_comm || ' ' || v_dname);
	
END;
/