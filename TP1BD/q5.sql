DECLARE
	CURSOR c IS select sal, empno, ename FROM emp
		WHERE sal > 4000
		START WITH empno = 7902 CONNECT BY PRIOR MGR = empno;
	v_sal	NUMBER;
	v_empno	NUMBER;
	v_ename	VARCHAR(25);

BEGIN
	OPEN c;
		
	FETCH c INTO v_sal, v_empno, v_ename;
	INSERT INTO temp VALUES (v_sal, v_empno, v_ename);

	CLOSE c;

END;
/

