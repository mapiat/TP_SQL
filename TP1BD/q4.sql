DECLARE
	CURSOR c IS select sal, empno, ename FROM emp WHERE (sal + NVL(comm, 0)) > 2000;
	v_sal	NUMBER;
	v_empno	NUMBER;
	v_ename	VARCHAR(25);

BEGIN
	OPEN c;
	LOOP
		FETCH c INTO v_sal, v_empno, v_ename;
		EXIT WHEN(c%NOTFOUND);
		INSERT INTO temp VALUES (v_sal, v_empno, v_ename);
	END LOOP;
	CLOSE c;

END;
/