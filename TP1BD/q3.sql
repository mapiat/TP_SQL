DECLARE
	CURSOR c IS select sal, empno, ename FROM emp ORDER BY sal DESC;
	v_sal	NUMBER;
	v_empno	NUMBER;
	v_ename	VARCHAR(25);

BEGIN
	OPEN c;
	FOR i in 1..5 LOOP
		FETCH c INTO v_sal, v_empno, v_ename;
		INSERT INTO temp VALUES (v_sal, v_empno, v_ename);
	END LOOP;
	CLOSE c;

END;
/