DECLARE
	col3	VARCHAR(25);

BEGIN
	FOR i IN 1..10 LOOP
		IF MOD(i, 2) = 0 THEN
			col3 := i || ' est pair';
		ELSE
			col3 := i || ' est impair';
		END IF;

		INSERT INTO temp VALUES(i, i * 100, col3);

	END LOOP;
END;
/